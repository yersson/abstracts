package com.jmendozadi;

public class Circle extends Geometric {
    public Circle(String figurename, int sidesnumber, double side) {
        super(figurename, sidesnumber, side);
    }

    @Override
    public void areaCalculation()
    {

    }

    @Override
    public void perimeterCalculation() {

    }

    @Override
    public void radiusCalculation() {
        radius=this.diameter/2;
    }
}
